var wordsFound = 0;

$("#foundRule").attr("style", "display: none;")

$("#attempt").keyup(function() {
    if ($("#attempt").val() == "") {
        $("#status").text("");
        $("#foundRule").attr("style", "display:none")
    } else {
        $.getJSON("/_check", {"text": $("#attempt").val()}, function(data) {
            if (data.result.newWord == 1) {
                $("#status").text("New word found!")
                $("#results").append($("#attempt").val() + "<br\>")
                $("#attempt").val('')
                wordsFound++;
                if (wordsFound == 1) {
                    $("#wordsfound").attr("style", "display: inline;")
                }
                if (wordsFound == 3) {
                    $("#attempt").prop("disabled", true)
                    $("#attempt").blur()
                    $("#status").text("All three words found. Refresh to play again")
                }
                $("#foundRule").attr("style", "display: block;")
            } else if (data.result.alreadyFound == 1) {
                $("#status").text("You already found that word")
            } else if (data.result.matched == 0 && data.result.inJumble == 1) {
                $("#status").text("Word not matched, but is in the jumble")
            } else if (data.result.inJumble == 0 && data.result.matched == 0) {
                $("#status").text("Word not in jumble, and unmatched")
            } else if (data.result.matched == 1 && data.result.inJumble == 0) {
                $("#status").text("Word was matched, but not in the jumble")
            }
        });
    }
})